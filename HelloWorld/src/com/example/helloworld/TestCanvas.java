package com.example.helloworld;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class TestCanvas extends SurfaceView implements SurfaceHolder.Callback {
	private CanvasThread canvasthread;
	private float deltaX=0, deltaY=0;
	private float newX=0, newY=0;
	private float startX=0, startY=0;
	private float deltaMaxX=50f, deltaMaxY=50f;
	private float touchProportion = 0.25f;
	
	public TestCanvas(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		setFocusable(true);
		getHolder().addCallback(this);
        canvasthread = new CanvasThread(getHolder(), this);
        
	}

	@Override
	public void onDraw(Canvas c) {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        c.drawPaint(paint);
        
        Bitmap kangoo = BitmapFactory.decodeResource(getResources(),
                        R.drawable.kangaroo);
        if( newX > c.getWidth() ) newX = -kangoo.getWidth();
        else if ( newX < -kangoo.getWidth() ) newX = c.getWidth();
        else newX += deltaX;
        if( newY > c.getHeight() ) newY = -kangoo.getHeight();
        else if ( newY < -kangoo.getHeight() ) newY = c.getHeight();
        else newY += deltaY;
        
        c.drawBitmap(kangoo, newX, newY, null);
        
        paint.setTextSize(30);
        paint.setColor(Color.WHITE);
        c.drawText("X: " + this.deltaX, 10f, 30f, paint);
        c.drawText("Y: " + this.deltaY, 10f, 55f, paint);
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		canvasthread.setRunning(true);
        canvasthread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		boolean retry = true;
        canvasthread.setRunning(false);
        while (retry) {
                try {
                        canvasthread.join();
                        retry = false;
                } catch (InterruptedException e) {
                        // we will try it again and again...
                }
        }
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		System.out.println("Touch Event: " + event.getX() + ", " + event.getY());
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			this.startX = (int)event.getX();
			this.startY = (int)event.getY();
			this.deltaX = 0;
			this.deltaY = 0;
			break;
		case MotionEvent.ACTION_MOVE:
		case MotionEvent.ACTION_UP:
			deltaX = (event.getX() - this.startX)*touchProportion;
			if( deltaX > deltaMaxX ) deltaX=deltaMaxX;
			deltaY = (event.getY() - this.startY)*touchProportion;
			if( deltaY > deltaMaxY ) deltaY=deltaMaxY;
			break;
		}
		return true;
	}
}
